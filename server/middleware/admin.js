const {User} = require('./../models/user');

let admin = (req, res, next) => {

    if (req.user.role === 0) return res.json({
        isAdmin: false
    })
    next();
}

module.exports = {admin}